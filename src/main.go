package main

import (
	"fmt"
	"syscall/js"
)

var ciProjectId string
var ciMergeRequestIid string
var ciProjectPath string
var ciCommitRefSlug string

func Html(_ js.Value, args []js.Value) interface{} {

	var visualReviewScript string
	/*
	  # Visual Reviews
    https://docs.gitlab.com/ee/ci/review_apps/#visual-reviews
		CI_PROJECT_ID
		CI_MERGE_REQUEST_IID
		CI_PROJECT_PATH
	*/
	
	if ciMergeRequestIid != "" {
		visualReviewScript = fmt.Sprintf(`
		<script
				data-project-id="%v"
				data-merge-request-id="%v"
				data-mr-url="https://gitlab.com"
				data-project-path="%v"
				data-require-auth="true"
				id="review-app-toolbar-script"
			 src="https://gitlab.com/assets/webpack/visual_review_toolbar.js">
		</script>
		`, ciProjectId, ciMergeRequestIid, ciProjectPath)
	} else {
		visualReviewScript = ""
	}

	htmlPage := fmt.Sprintf(`
	<!doctype html>
	<html>
		<head>
			<meta charset="utf-8">
			<meta http-equiv="x-ua-compatible" content="ie=edge">
			<title>Hello World!</title>
			<meta name="description" content="">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			
			<style>
				.container { min-height: 100vh; display: flex; justify-content: center; align-items: center; text-align: center; }
				.title { font-family: "Source Sans Pro", "Helvetica Neue", Arial, sans-serif; display: block; font-weight: 300; font-size: 100px; color: #35495e; letter-spacing: 1px; }
				.subtitle { font-family: "Source Sans Pro", "Helvetica Neue", Arial, sans-serif; font-weight: 300; font-size: 42px; color: #526488; word-spacing: 5px; padding-bottom: 15px; }
				.links { padding-top: 15px; }
			</style>

			<!--
			# Visual Reviews
			https://docs.gitlab.com/ee/ci/review_apps/#visual-reviews
			-->
      %v

		</head>
		<body>
			<section class="container">
				<div>
					<h1 class="title">
					👋 Hello World 🌍
					</h1>
					<h1 class="title">
					👋 Hello World 🌍
					</h1>
					<h2 class="subtitle">
					made with 💚 🍵 and WebAssembly
					</h2>    
					<h3 class="subtitle">
					made with 💚 🍵 and GoLang
					</h3>            
				</div>
			</section>
			<script>
				let version = "%v";
				
				document.querySelector("h3").innerText = "🖐 branch: " + version
			</script>
		</body> 
	</html>  	
	`, visualReviewScript, ciCommitRefSlug)
	return htmlPage
}

func main() {

	js.Global().Set("Html", js.FuncOf(Html))

	<-make(chan bool)
}
